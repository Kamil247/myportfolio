var result,
    btnVal,
    valResult,
    res,
    lastOperation,
    memory,
    className = ['operator'],
    classNameMemory = ['operator'];
    
//check all conditions

 function result(){
    if(btnVal == 'CE'){
        remove();
    }else if(btnVal == 'Backspace' ){
        removeLast();
    }else if(className[0] == classNameMemory[0] && className[0] == 'operator' ){
        checkOperators()
    }else if(btnVal == 'Enter' ){
        score();
    }else{
        updateDisplay()
    }
    valResult = document.getElementById("result").innerText;
    dis(); 
 }

//buttons clicks
document.querySelectorAll('button').forEach(item => {
item.addEventListener('click', event => {
        memory = btnVal;
        classNameMemory = className;
        btnVal = item.value;
        className = item.classList;
        valResult = document.getElementById("result").innerText;
        result();
    })
});

//keyboard clicks
document.addEventListener('keydown', (event) => {
    const keyName = event.key;
    buttonName = document.querySelectorAll('button');
    console.log(keyName);
    for(var i = 0; i < buttonName.length; i++){
        if(keyName == buttonName[i].value){
            buttonName[i].click();
        }
    }
  });

//distable buttons if calc is empty 
function dis(){
    if(valResult == ''){ 
         Array.from(document.getElementsByClassName("disabled")).forEach(function(item) {
            item.setAttribute("disabled","true")
         });
    }else if(valResult[0] == '-' && valResult[1] == undefined ){
        Array.from(document.getElementsByClassName("disabled")).forEach(function(item) {
            item.setAttribute("disabled","true")
         });
    }
    else{
        Array.from(document.getElementsByClassName("disabled")).forEach(function(item) {
            item.removeAttribute("disabled")
         });
    }
}
//clear display
function remove(){
    document.getElementById("result").innerHTML = '';
    dis();
}
function removeLast(){
    valResult = valResult.substring(0, valResult.length - 1);
    document.getElementById("result").innerHTML = valResult;
}
//change operators and do not duplicate it
function checkOperators(){
    valResult = valResult.substring(0, valResult.length - 1);
    valResult += btnVal
    document.getElementById("result").innerHTML = valResult;
}
//start calculating
function score(){
    var lastChar = valResult.charAt(valResult.length - 1)
    if(lastChar == "*" || lastChar == "/" || lastChar == "-" || lastChar == "+"|| lastChar == "."){
        alert("Działanie musi zostać zakończone liczbą");
        removeLast()
    }else{
        lastOperation = valResult;
        res = eval(valResult);
        document.getElementById("lastOperation").innerHTML = lastOperation + " = " + res;
        document.getElementById("result").innerHTML = res;
        console.logz
    }
}
//update display
function updateDisplay(){
    valResult += btnVal
    document.getElementById("result").innerHTML = valResult;
}