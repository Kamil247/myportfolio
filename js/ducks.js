$(document).ready(function(){

  $('.target').attr('draggable', false);
  
  //  global variables
    var animation = 1,
      status = true,
      speed,
      randomH,
      ramdomW,
      score = 0,
      shots = 12,
      hit = 6,
      level = 1,
      levelName = easy,
      randomW,
      randomH,
      scoreVal = 500,
      insert = true,
      resize = 1;

  //Sounds

  var shotSound = new Audio();
  shotSound.src = '../images/shot.mp3';
  var nextLevelSound = new Audio();
  nextLevelSound.src = '../images/level.mp3';
  var gameOverSound = new Audio();
  gameOverSound.src = '../images/gameOver.mp3'
  //random functions

  random();

  function random(min, max){
    min = Math.ceil(0);
    max = Math.floor(70);
    randomW = Math.floor(Math.random() * (max - min)) + min;
  }

  function moveLeft(min, max){
    $('.random-block').css("left", randomW + "%");
    console.log('left' + randomW);
  }

  function moveTop(min, max){
    min = Math.ceil(0);
    max = Math.floor(70);
    randomH = Math.floor(Math.random() * (max - min)) + min;
    console.log('top' + randomH);
    i = randomH;
  }

  /// Start game / Init
  
    $('.level').click(function(){
      array = $(this).data("atr");
      $('.level').addClass('hidden');
      $('#start').removeClass('hidden');
      $('#scores').addClass('hidden')
      shots = array[0];
      speed = array[1];
      levelName = array[2];
    });

    $('#start').click(function(){
      moveTop();
      moveLeft();
      status = true;
      right();
      $( ".random-block" ).animate({top: randomH + "%"}, 500 );
      $('.shots').html(shots);
      $('.hits').html(hit);
      $(this).addClass('hidden');
      $('.buttons').addClass('hidden')
      restart();
    });

  //Animations 

    function right(){
      if(status == true){
        console.log(resize);
        $( ".target" ).css({transform: "rotateY(0deg)"});
        $( ".random-block" ).animate({
          left: randomW + '%',
          top: randomH + '%'
          }, 200 , "linear" , function (){
          randomW = randomW + resize + speed;
          animation = 0;
          console.log(randomW);
          if(randomW > 105){
            moveTop();
            $('.random-block').css("top", randomH +"%");
            left();
          }else{
            right();  
          }   
          if(status == false){}    
          }
        )};
      }
      function left(){
        if(status == true){
          console.log(resize);
          $( ".target" ).css({transform: "rotateY(180deg)"});
        $( ".random-block" ).animate({
          left: randomW + '%',
          top: randomH + '%'
        }, 200 ,"linear", function (){
        animation = 1;
        console.log(randomW);
        randomW = randomW - resize - speed;
        if(randomW < -5){
          moveTop();
          $('.random-block').css("top", randomH +"%");
          right();
        }else{
          left();
        }
        if(status == false){
        }       
        }
      )}};


    // Feature
    // function upright(){
    //   if(status == true){
    //     $( ".random-block" ).animate({
    //     }, 400 , function (){
    //     if(i == randomH + 3){
    //       i = randomH - 3 ;
    //       upright();
    //     }else{
    //       i = randomH + 3;
    //       upright();
    //     }
    //     }
    //   )}
    // }

    // Function to check width and change speed

      function windowCheck() {
      if ($(window).width() > 1100) {
        resize = 1;
      }
      else if ($(window).width() > 800) {
        resize = 3;
      }
      else if ($(window).width() > 600) {
        resize = 5;
      }
      else if ($(window).width() > 400) {
        resize = 8;
      }
      else if ($(window).width() > 300) {
        resize = 10;
      }
    }
    
    //Updates

    $('body').click(function(){
      arrayUpdate();
      windowCheck();

    })

    //Shots and logic

    $('.game').click(function(){
      shots--;
      shotSound.play();
      gameOver(); 
    });

    $('.headshot').click(function(){
      scoreVal = 1000;
      score = score + 500;
    });
   
    $('.target').click(function(){
      scoreShot();
      
      scoreVal = 500;
      score = score + 500;
      hit--;
      status = false;
      gameOver(); 
      $( ".target" ).css({backgroundImage:'url(../images/duck3.png)', animation: 'none', backgroundSize: '73px 93px' });
      $( ".random-block" ).animate({ top: "100%" }, 500, function(){
        random();
        moveLeft();
        moveTop();
        $( ".target" ).css({animation: '1s duck infinite cubic-bezier(1, 0, 0, 1)', backgroundSize: '100px 75px' });
        $( ".random-block" ).animate({ top: randomH + "%" }, 500 );
        status = true;
        if(animation == 1){
          left();
        }else{
          right();
        }
      });

      ///NEXT LEVEL

      if(hit == 0){
        nextLevelSound.play();
        level++;
        speed++;

        $( "#nextLevel" ).text( "LEVEL" + level);
        $( ".popup-level" ).slideDown( 2000, function() {
          $( ".popup-level" ).slideUp( 2000);
        });
        restart()
        }
      });

      /// GAME OVER

      function gameOver(){
        if(shots == 0 && hit < 7){
          showUser();
          level = 1;
          gameOverSound.play();
          $('.random-block').css("top", "100%");
          $('.popup').addClass('visible');
          status = false;
        }
      }

    //Flying points

    function scoreShot(){
      $('.points').text(scoreVal)
      $('.points').css("top" , randomH +"%")
      $('.points').css("left", randomW + "%")
      $('.points').css("display" , "flex")
      $('.points').animate({
        top: randomH - 6 + '%',   
      },1000 , function(){
        $('.points').css("display" , "none")
      })
    }

    //Updates // Restart

    function arrayUpdate(){
      $('.hits').html(hit);
      $('.score-val').html(score);
      $('.shots').html(shots);
    }

    function restart(){
      shots = array[0];
      hit = 6;
    }

    //Buttons

    $('.exit').click(function(){
      showUser();
      score = 0;
      $('.popup').removeClass('visible');
      $('#scoreForm').removeClass('hidden');
      $('#scores').removeClass('hidden');
      $('.level').removeClass('hidden');
      $('.buttons').removeClass('hidden');
    })    

    $('#scores').click(function(){
      showUser();
      $('#scoreForm').addClass('hidden');
      $('.popup').addClass('visible');
      $('.buttons').addClass('hidden'); 
    })

    // Send result to database (AJAX)

    $('#sendScore').click(function(){
      var name = $("input#name").val();
      var scoreData = {
          'name':name,
          'score':score,
          'levelName':levelName,
          'insert':insert,
      };
          $.ajax({
              url: "/ducks.php",
              data: scoreData,
            }).done(function() {
              alert('done');
              reset();
            });   
    })

    // Show Score form Database
    function showUser(str) {
      if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
      } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById("txtHint").innerHTML = this.responseText;
          }
        };
        xmlhttp.open("GET","ducks.php",true);
        xmlhttp.send();
      }
    }  
});