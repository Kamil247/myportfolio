$(document).ready(function(){

  random();
  $('.target').attr('draggable', false);
  showUser();

  //  global variables
    var millisecond = 100;
    var secound = 59;
    var minute = 0;
    var status = 0
    var randomH;
    var ramdomW;
    var score = 0;
    var difficulty
    var level = easy;
    var insert = true;

  // reset variables

  function reset(){
    status = false;
    millisecond = 99;
    secound = 59;
    minute = 0;
    score = 0;
  }

  //  levels of difficulty and size of boxes

    $('.difficulty').click(function(){
      $('.difficulty').css('background-color', 'grey')
      array = $(this).data("atr");
      level = array[2];
      $('.target').css('width',array[0]);
      $('.target').css('height',array[0]);
      $('.target').css('animation-name',array[1]);
      $(this).css('background-color', 'green')
    })
    
  // Buttons
    $('#start').click(function(){
      if( $('#start').hasClass('run')){
        $(this).removeClass('run');
        $(this).text('START');
        status = false;
        $('.random-block').removeClass('interactive');

        
      }else{
        $(this).addClass('run');
        milliseconds();
        status = true;
        $(this).text('PAUSE');
        $('.random-block').addClass('interactive');
        $('.difficulty').prop("disabled", true);
      }
    });

    $('#restart').click(function(){
        $('#start').removeClass('run');
        $('.random-block').removeClass('interactive');
        reset();
        $('.difficulty').prop("disabled", false);
        $('.count').html(score);
        $('.time').html("1:0:0");
        $('#start').text('START');
    });

    $('.target').click(function(){
      random();
      score++;
      $('.count').html(score);
    });

    $('.game').click(function(){
      random();
    });

    $('#scores').click(function(){
      showUser();
      $('#scoreForm').addClass('hidden');
      $('.popup').addClass('visible');
      $('#start').removeClass('run');
      $('.random-block').removeClass('interactive');
      reset();
      $('.difficulty').prop("disabled", false);
      $('.count').html(score);
      $('.time').html("1:0:0");
      $('#start').text('START');
    })

    $('.exit').click(function(){
      showUser();
      score = 0;
      $('#scoreForm').removeClass('hidden');
      $('.popup').removeClass('visible');

    })


     // Timer

    function milliseconds(){
      setTimeout(function(){
        millisecond--;
        if(status == true){
          if(millisecond == 0){
            secounds();
            millisecond = 100;
          }else{
            milliseconds();
            $('.time').html(minute + ":" + secound + ":" + millisecond);
          }
        }
      }, 1);
    }

    function secounds(){
        secound--;
        if(secound == 0){
          $( "#start" ).trigger( "click" );
          $('.difficulty').prop("disabled", false);
          $('.popup').addClass('visible');   
          status = false;
          $('.time').html("1:0:0");
        }else{
          milliseconds();
        }
    }
    /// Random blocks

    function random(min, max) {
      min = Math.ceil(0);
      max = Math.floor(87);
      randomW = Math.floor(Math.random() * (max - min)) + min;
      randomH = Math.floor(Math.random() * (max - min)) + min;
      $('.random-block').css("left", randomW + "%");
      $('.random-block').css("top", randomH + "%");
    }

    // Send result to database (AJAX)

      $('#sendScore').click(function(){
        var name = $("input#name").val();
        var scoreData = {
            'name':name,
            'score':score,
            'level':level,
            'insert':insert,
        };
            $.ajax({
                url: "/scoreboard.php",
                data: scoreData,
              }).done(function() {
                alert('done');
                reset();
              });   
      })

      function showUser(str) {
        if (str == "") {
          document.getElementById("txtHint").innerHTML = "";
          return;
        } else {
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              document.getElementById("txtHint").innerHTML = this.responseText;
            }
          };
          xmlhttp.open("GET","scoreboard.php",true);
          xmlhttp.send();
        }
      }  
});