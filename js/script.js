
var projects = document.getElementsByClassName('project'),
    xPosition,
    yPosition,
    imgPosition,
    textPosition,
    shadowPosition;

Array.from(projects).forEach(element => {
    element.addEventListener('mouseover', event => {
        element.lastElementChild.style.transitionDuration = "0ms";
    })

    element.addEventListener('mouseleave', event => {
        element.lastElementChild.style.transitionDuration = "400ms";
        element.lastElementChild.style.transform = "perspective(0px) rotateX(0deg) rotateY(0deg) scale3d(1, 1, 1)";
        element.className = "project";
        element.firstElementChild.className = "live";
        element.lastElementChilgd.style.filter = "";
    })

    element.addEventListener('mousemove', event => {
        showCoords(event);
    })

    element.addEventListener('click', event => {
        element.lastElementChild.style.transitionDuration = "400ms";
        element.style.transitionDuration = "500ms";
        element.className = "project expanded";
        element.firstElementChild.className = "live visible";
        element.lastElementChild.style.filter = "blur(1px) brightness(0.7)";
    })

    function showCoords(event) {
        xPosition = (event.clientX - element.offsetLeft - (element.offsetWidth * 0.5))*(-0.05);
        yPosition = (event.pageY - element.offsetTop - (element.offsetHeight * 0.5))*0.05;
        imgPosition = "perspective(1000px) rotateX("+yPosition+"deg) rotateY("+xPosition+"deg) scale3d(1, 1, 1)"
        textPosition = "perspective(1000px) rotateX("+yPosition+"deg) rotateY("+xPosition+"deg) scale3d(1, 1, 1) translate3d("+xPosition+5+"px, "+yPosition*(-2)+"px, 0px)"
        shadowPosition = xPosition+"px "+yPosition*(-1)+"px 12px black"
        element.firstElementChild.firstElementChild.style.transform = textPosition;
        element.firstElementChild.firstElementChild.style.textShadow = shadowPosition;
        element.lastElementChild.style.transform = imgPosition;
      }
})

$(document).ready(function(){

    var typedText = `Hi,<br>My name is Kamil. <br> What You want to know ?`

    $('.chat-action').click(function(){
        $('#chat-span').html(`<span class='chat-span'></span>`);
        $(this).addClass('hidden')
        typedText = $(this).val();
        $('.block').prop('disabled', true);
        chat();
        if($('.chat .option').length === $('.chat .option.hidden').length){
            $('#moreInfo').removeClass('hidden');
            $('.option').addClass('hidden')
        }
    });
    $('#moreInfo').click(function(){
        $('#moreInfo').addClass('hidden');
        $('.choice').removeClass('hidden');
    })
    
    // auto typing
    function chat(){
        var typed5 = new Typed('.chat-span', {
            strings: [ typedText ],
            typeSpeed: 1,
            backSpeed: 1,
            loop: false
          });
    }
    chat();
      var typed2 = new Typed('.typed2', {
        strings: ['My skills', 'My way to being a front-end', 'My way to being a <strong> front-end </strong> developer'],
        typeSpeed: 100,
        backSpeed: 60,
        startDelay: 2000,
        loop: false
      });
      var typed4 = new Typed('#text', {
            strings: ['Hi. We are interested in cooperation. Call Us', 'We liked your site. We sent an email', 'I have an offer for you. We are waiting for answer'],
            typeSpeed: 30,
            backSpeed: 30,
            attr: 'placeholder',
            bindInputFocusEvents: true,
            loop: true
      });
     

    //Slider
    var swiper = new Swiper('.swiper-container', {
        effect: 'coverflow',
        autoplay: {
            delay: 2000,
            disableOnInteraction: false,
          },
          loop: true,
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows : true,
      },
      pagination: {
        el: '.swiper-pagination',
      },
      });
    
    //Menu Scrolling

    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();
    
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
    });

    
    //Show/Hide menu mobile
    $(".bnt-hover").click(function(){
        if( $(this).hasClass('expanded')){
            $(this).removeClass('expanded')
            $('.menu').removeClass('menu-expanded');
        }
        else{
            $(this).addClass('expanded')
            $('.menu').addClass('menu-expanded');
        }
    });

    $('.btn-menu').click(function(){
        $('.menu').removeClass('menu-expanded');
        $('.bnt-hover').removeClass('expanded');
    });

  
    $("#contactForm").validate({
        rules: {
            name: "required",
            company: {
                required: false,
                minlength: 2
            },
            email: {
                required: true,
                minlength: 5
            },
            phone: {
                required: false,
                minlength: 7,
            },
        },
        messages: {
            name: "",
            surname: "",
            company: {
                required: "",
                minlength: ""
            },
            phone: "",
            email: "",
        },
        submitHandler: function() {
            var name = $("input#name").val();
            var company = $("input#company").val();
            var email = $("input#email").val();
            var phone = $("input#phone").val();
            var text = $("textarea#text").val();
            var oData = {
                'name':name,
                'email':email,
                'phone':phone,
                'company':company,
                'text':text
            };
                $.ajax({
                    url: "contact.php",
                    data: oData,
                  }).done(function() {
                    alert('Massage successful send');
                  });   
            }
    });
    
});